<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','Front\HomeController@index');
Route::get('/hakkimizda','Front\HomeController@about');
Route::get('/maldiv-otelleri','Front\HotelController@listHotel');
Route::get('/misafir-yorumlari','Front\HomeController@misafirYorumlari');
Route::get('/iletisim','Front\HomeController@contact');
Route::post('/send-email','Front\HomeController@sendEmail');

Route::post('/hotel-query','Front\HomeController@hotelQuery');

Route::post('/filtrele', 'Front\HomeController@filtrele');
Route::post('/maldiv-otelleri/filtrele', 'Front\HotelController@listHotelfiltre');
Route::get('/{title}/{id}','Front\HotelController@showHotel');
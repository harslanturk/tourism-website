<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SiamForms extends Model
{
    protected $table = 'siam_forms';
    protected $fillable = [
        'ad', 'telefon', 'email', 'konu', 'mesaj', 'gidis', 'donus', 'baslik', 'ek_aciklama', 'status',
    ];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SiamMyorum extends Model
{
    protected $table = 'myorum';
    protected $fillable = [
       'gonderen', 'tur', 'mesaj', 'kategori', 'tarih', 'sira', 'status',
    ];
}

<?php
/**
 * Created by PhpStorm.
 * User: Hasan
 * Date: 17.10.2016
 * Time: 14:35
 */
namespace App\Helpers;
use App\User;
use Session;
use App\Modules;
use Auth;
use App\UserDelegation;
use Carbon\Carbon;
use App\UserCustomers;
use App\Kur;
use App\Notification;
use DB;
use App\SiamForms;



class Helper
{
    public static function shout($url)
    {
        $url = explode('/', $url);
        $modul = '/'.$url['0'].'/'.$url['1'];

        $id=Modules::where('url',$modul)->select('id')->first()->id;
        $sess=Session::get($id );
        return $sess;
    }

    public static function vipShout($url)
    {
        /*$url = explode('/', $url);
        $modul = '/'.$url['0'].'/'.$url['1'];*/

        $id=Modules::where('url',$url)->select('name')->first()->name;
        $sess=Session::get($id );
        return $sess;
    }

    public static function sessionReload()
    {
        //Session::flush();
        $del_id=Auth::user()->delegation_id;
        $delegation=UserDelegation::where('id',$del_id)->select('auth')->first();

        $json=json_decode($delegation->auth);


        foreach ($json as $key => $str) {
            $id = Modules::where('id', $key)->select('id')->first()->id;
            $read = substr($str, 0, 1);
            $add = substr($str, 1, 1);
            $update = substr($str, 2, 1);
            $delete = substr($str, 3, 1);

            $data = array(
                'r' => $read,
                'a' => $add,
                'u' => $update,
                'd' => $delete,
            );
            Session::put($id, $data);
        }
    }
    public static function MonthNameConverter($oldMonth)
    {
        $search  = array('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12');
        $replace = array('OCAK', 'ŞUBAT', 'MART', 'NİSAN', 'MAYIS', 'HAZİRAN', 'TEMMUZ', 'AĞUSTOS', 'EYLÜL', 'EKİM', 'KASIM'    , 'ARALIK');
        $newMonth=str_replace($search, $replace, $oldMonth);
        return $newMonth;
    }
    public static function TrConvert($keyword)
    {
        $tr = array('ş','Ş','ı','İ','ğ','Ğ','ü','Ü','ö','Ö','Ç','ç');
        $eng = array('s','s','i','i','g','g','u','u','o','o','c','c');
        $keyword = str_replace($tr,$eng,$keyword);
        $keyword = strtolower($keyword);
        $keyword = preg_replace('/&.+?;/', '', $keyword);
        $keyword = preg_replace('/[^%a-z0-9 _-]/', '', $keyword);

        /*$keyword = preg_replace('/s+/', '-', $keyword);
        $keyword = preg_replace('|-+|', '-', $keyword);*/
        $keyword = trim($keyword, '-');
        $keyword = str_slug($keyword);

        return $keyword;
    }
    public static function DateConvertDMY($dt)
    {
        return date_format (date_create ($dt), 'd-m-Y');
    }
    public static function DateConvertYMD($dt)
    {
        return date_format (date_create ($dt), 'Y-m-d');
    }
    // Zaman farkını türkçe olarak geri döndürme.
    public static function trDiff($tarih)
    {
        Carbon::setLocale('tr');
        $result = Carbon::parse($tarih)->diffForHumans();
        return $result;
    }

    public static function carbonFormat($tarih,$format)
    {
        Carbon::setLocale('tr');
        $result = Carbon::parse($tarih)->format($format);
        return $result;
    }

    public static function events($user_id)
    {
        $now = Carbon::now();
        $result = Event::where('user_id',$user_id)->where('end_time','>',$now)->orderBy('created_at','desc')->take(5)->get();
        return $result;
    }

    public static function get_modules()
    {
        $modules = Module::where('status','1')
                        ->where('parent_id','0')
                        ->orderBy('priority','asc')
                        ->get();

        return $modules;
    }

    public static function get_par_modules()
    {
        $par_modules = Module::where('status','1')
                        ->where('parent_id','>','0')
                        ->orderBy('priority','asc')
                        ->get();

        return $par_modules;
    }

    public static function last_dosyano()
    {
        $last_dosyano = UserCustomers::where('dosyano', '!=', '')->select('dosyano')->orderBy('dosyano', 'DESC')->first()->dosyano;

        return $last_dosyano;
    }

    public static function last_kur()
    {
        $last_kur = Kur::orderBy('tarih', 'DESC')->first();

        return $last_kur;
    }

    public static function first_day_of_month()
    {
        $start = new Carbon('first day of this month');
        Carbon::setLocale('tr');
        $result = Carbon::parse($start)->format("Y-m-d");

        return $result;

    }

    public static function last_day_of_month()
    {
        $end = new Carbon('last day of this month');
        Carbon::setLocale('tr');
        $result = Carbon::parse($end)->format("Y-m-d");

        return $result;

    }

    public static function later_today_of_month($month)
    {
        $result = Carbon::now()->addMonths($month);
        Carbon::setLocale('tr');
        $result = Carbon::parse($result)->format("Y-m-d");

        return $result;

    }

    public static function day_of_month($month, $yil)
    {
        $start = new Carbon('first day of '.$month.' '.$yil);
        $end = new Carbon('last day of '.$month.' '.$yil);
        Carbon::setLocale('tr');
        $result['first'] = Carbon::parse($start)->format("Y-m-d");
        $result['end'] = Carbon::parse($end)->format("Y-m-d");
        return $result;
    }

    public static function first_end_day_of_month($yil)
    {
        $start = new Carbon('first day of January'.' '.$yil);
        $end = new Carbon('last day of December'.' '.$yil);
        Carbon::setLocale('tr');
        $result['first'] = Carbon::parse($start)->format("Y-m-d");
        $result['end'] = Carbon::parse($end)->format("Y-m-d");
        return $result;
    }

    public static function DateConvertTurkishDMY($dt)
    {
        $aylar = array( 
            'January'    =>    'Ocak', 
            'February'    =>    'Şubat', 
            'March'        =>    'Mart', 
            'April'        =>    'Nisan', 
            'May'        =>    'Mayıs', 
            'June'        =>    'Haziran', 
            'July'        =>    'Temmuz', 
            'August'    =>    'Ağustos', 
            'September'    =>    'Eylül', 
            'October'    =>    'Ekim', 
            'November'    =>    'Kasım', 
            'December'    =>    'Aralık', 
            'Monday'    =>    'Pazartesi', 
            'Tuesday'    =>    'Salı', 
            'Wednesday'    =>    'Çarşamba', 
            'Thursday'    =>    'Perşembe', 
            'Friday'    =>    'Cuma', 
            'Saturday'    =>    'Cumartesi', 
            'Sunday'    =>    'Pazar', 
        );  
        $data = date_format (date_create ($dt), 'd F Y');
        $data = strtr($data, $aylar); 
        return $data;
    }

    public static function getNotification()
    {
        $user = Auth::user();
        $data = Notification::where('status', '1')->where('user_id', '0')->orWhere('user_id', $user->id)->orderBy('id','DESC')->get();
        $dataCount = Notification::where('status', '1')->where('user_id', '0')->orWhere('user_id', $user->id)->count();

        return $data;
    }

    public static function countNotification()
    {
        $user = Auth::user();
        $dataCount = Notification::where('status', '1')->where('user_id', '0')->orWhere('user_id', $user->id)->count();

        return $dataCount;
    }

    public static function saveNotification($user_id,$type,$url,$status,$content="")
    {
        if($type == "new-customers")
          {
            $liStyle = "fa-user text-aqua";
            $content = "Yeni bir müşteri eklendi.";
          }
          else if($type == "new-contact-form")
          {
            $liStyle = "fa-envelope text-yellow";
            $content = "Yeni bir iletişim formu geldi."; 
          }
          else if($type == "new-katalog")
          {
            $liStyle = "fa-book text-purple";
            $content = "Yeni bir katalog talebi geldi."; 
          }
          else if($type == "new-siamclub")
          {
            $liStyle = "fa-address-card text-green";
            $content = "Yeni bir siamclub başvuru talebi geldi."; 
          }
          /*else if($type == "new-system")
          {
            $liStyle = "fa-address-card text-red";
            $content = "Yeni bir siamclub başvuru talebi geldi."; 
          }*/
          else
          {
            $liStyle="fa-info-circle text-aqua";
          }
        $data = new Notification();
        $data->user_id = $user_id;
        $data->type = $type;
        $data->url = $url;
        $data->status = $status;
        $data->content = $content;
        $data->li_style = $liStyle;
        $data->save();
    }

    public static function countBirthday()
    {

        //SELECT * FROM musteri_detay WHERE MONTH(dogumtarihi) = MONTH(NOW()) AND DAY(dogumtarihi) = DAY(NOW());
        $data = DB::table('siamtur_crm.musteri_detay')
                ->where(DB::raw("MONTH(dogumtarihi)"), DB::raw("MONTH(NOW())"))
                ->where(DB::raw("DAY(dogumtarihi)"), DB::raw("DAY(NOW())"))
                ->orderBy('siamtur_crm.musteri_detay.id', 'DESC')
                ->select('siamtur_crm.musteri_detay.*', DB::raw("COUNT(siamtur_crm.musteri_detay.id) as dogum_say"))
                ->count();
        /*echo "<pre>";
        print_r($data);
        die();*/


        return $data;
    }
    public static function randColor() {
        return '#' . str_pad(dechex(mt_rand(0, 0xFFFFFF)), 6, '0', STR_PAD_LEFT);
    }

    public static function dateKur($tarih, $fiyat)
    {
        $last_kur = Kur::where('tarih', $tarih)->first();
        $sonuc = $fiyat * $last_kur->dolareuro;
        return $sonuc;
    }

    public static function saveContactForm($ad, $telefon, $email, $konu, $mesaj, $gidis, $donus, $baslik, $ek_aciklama)
    {
        $data = new SiamForms();
        $data->ad = $ad;
        $data->telefon = $telefon;
        $data->email = $email;
        $data->konu = $konu;
        $data->mesaj = $mesaj;
        $data->gidis = $gidis;
        $data->donus = $donus;
        $data->baslik = $baslik;
        $data->ek_aciklama = $ek_aciklama;
        $data->save();
    }

}
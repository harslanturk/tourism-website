<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hotel extends Model
{
  protected $table = 'oteller';
  protected $fillable = [
    'otel_ad','konum','olanaklar','odalar','adres','aciklama','lokasyon','destinasyon','yildiz','5yildiz','4yildiz','3yildiz','oda_kahvalti','yarim_pansiyon','tam_pansiyon','hersey_dahil','deniz_ucagi','surat_motoru','su_ustu','kumsal','sira','status',
  ];

}

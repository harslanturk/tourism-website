<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Hotel;
use App\HotelResim;
use DB;
use App\SiamMyorum;
use Mail;
use App\Helpers\helper;

class HomeController extends Controller
{
    public function index()
    {
    	$data = Hotel::where('status', 1)->where('destinasyon', 1)->orderBy('sira', 'ASC')->get();
        $myorum = SiamMyorum::where('kategori', 0)->orderBy('id', 'DESC')->get();
        $makaleUlti = DB::table('siamtur_db.makale')
            ->join('siamtur_db.wp_posts', 'siamtur_db.makale.wp_id', '=', 'siamtur_db.wp_posts.ID')
            ->join('siamtur_db.wp_postmeta as asd', 'siamtur_db.wp_posts.ID', '=', 'asd.post_id')
            ->join('siamtur_db.wp_postmeta', 'siamtur_db.wp_postmeta.post_id', '=', 'asd.meta_value')
            ->where('asd.meta_key', '_thumbnail_id')
            ->where('siamtur_db.wp_postmeta.meta_key', '_wp_attached_file')
            ->where('siamtur_db.makale.tur_kod', '35')
            ->groupBy('siamtur_db.makale.id')
            ->orderBy('siamtur_db.makale.id', 'asc')
            ->select('siamtur_db.wp_posts.post_name', 'siamtur_db.wp_posts.post_title', 'asd.post_id', 'asd.meta_key', 'siamtur_db.wp_postmeta.meta_value')
            ->get();
            /*where('kumsal', $data['kumsal'])
                    ->where('su_ustu', $data['su_ustu'])
                    ->where('surat_motoru', $data['surat_motoru'])
                    ->where('deniz_ucagi', $data['deniz_ucagi'])
                    ->where('hersey_dahil', $data['hersey_dahil'])
                    ->where('tam_pansiyon', $data['tam_pansiyon'])
                    ->where('yarim_pansiyon', $data['yarim_pansiyon'])
                    ->where('oda_kahvalti', $data['oda_kahvalti'])
                    ->where('3yildiz', $data['3yildiz'])
                    ->where('4yildiz', $data['4yildiz'])
                    ->where('5yildiz', $data['5yildiz'])*/
        $filtre=['5yildiz' => '0', '4yildiz' => '0', '3yildiz' => '0', 'oda_kahvalti' => '0', 'yarim_pansiyon' => '0', 'tam_pansiyon' => '0', 'hersey_dahil' => '0', 'deniz_ucagi' => '0', 'surat_motoru' => '0', 'su_ustu' => '0', 'kumsal' => '0'];

    	return view('home', ['data' => $data, 'myorum' => $myorum, 'makaleler' => $makaleUlti, 'filtre' => $filtre]);
    }

    public function filtrele(Request $request)
    {
    	$deger = $request->all();
        
    	//$filtre = "";
        if($request->has('5yildiz') != "")
        {
            $deger['5yildiz'] = 1;
            //$filtre = ['5yildiz' => '1'];
            $filtre['5yildiz'] = "1";
        }
        else
        {
            $deger['5yildiz'] = 0;
        }

        if($request->has('4yildiz') != "")
        {
            $deger['4yildiz'] = 1;
            $filtre['4yildiz'] = "1";
        }
        else
        {
            $deger['4yildiz'] = 0;
        }


        if($request->has('3yildiz') != "")
        {
            $deger['3yildiz'] = 1;
            $filtre['3yildiz'] = "1";
        }
        else
        {
            $deger['3yildiz'] = 0;
        }


        if($request->has('oda_kahvalti') != "")
        {
            $deger['oda_kahvalti'] = 1;
            $filtre['oda_kahvalti'] = "1";
        }
        else
        {
            $deger['oda_kahvalti'] = 0;
        }

        if($request->has('yarim_pansiyon') != "")
        {
            $deger['yarim_pansiyon'] = 1;
            $filtre['yarim_pansiyon'] = "1";
        }
        else
        {
            $deger['yarim_pansiyon'] = 0;
        }

        if($request->has('tam_pansiyon') != "")
        {
            $deger['tam_pansiyon'] = 1;
            $filtre['tam_pansiyon'] = "1";
        }
        else
        {
            $deger['tam_pansiyon'] = 0;
        }

        if($request->has('hersey_dahil') != "")
        {
            $deger['hersey_dahil'] = 1;
            $filtre['hersey_dahil'] = "1";
        }
        else
        {
            $deger['hersey_dahil'] = 0;
        }

        if($request->has('deniz_ucagi') != "")
        {
            $deger['deniz_ucagi'] = 1;
            $filtre['deniz_ucagi'] = "1";
        }
        else
        {
            $deger['deniz_ucagi'] = 0;
        }

        if($request->has('surat_motoru') != "")
        {
            $deger['surat_motoru'] = 1;
            $filtre['surat_motoru'] = "1";
        }
        else
        {
            $deger['surat_motoru'] = 0;
        }

        if($request->has('su_ustu') != "")
        {
            $deger['su_ustu'] = 1;
            $filtre['su_ustu'] = "1";
        }
        else
        {
            $deger['su_ustu'] = 0;
        }

        if($request->has('kumsal') != "")
        {
            $deger['kumsal'] = 1;
            $filtre['kumsal'] = "1";
        }
        else
        {
            $deger['kumsal'] = 0;
        }
        $filtre['status'] = 1;
        $filtre['destinasyon'] = 1;
        $data = Hotel::where($filtre)
                    /*where('kumsal', $data['kumsal'])
    				->where('su_ustu', $data['su_ustu'])
    				->where('surat_motoru', $data['surat_motoru'])
    				->where('deniz_ucagi', $data['deniz_ucagi'])
    				->where('hersey_dahil', $data['hersey_dahil'])
    				->where('tam_pansiyon', $data['tam_pansiyon'])
    				->where('yarim_pansiyon', $data['yarim_pansiyon'])
    				->where('oda_kahvalti', $data['oda_kahvalti'])
    				->where('3yildiz', $data['3yildiz'])
    				->where('4yildiz', $data['4yildiz'])
                    ->where('5yildiz', $data['5yildiz'])*/
    				->get();
    	$myorum = SiamMyorum::where('kategori', 0)->orderBy('id', 'DESC')->get();
        $makaleUlti = DB::table('siamtur_db.makale')
            ->join('siamtur_db.wp_posts', 'siamtur_db.makale.wp_id', '=', 'siamtur_db.wp_posts.ID')
            ->join('siamtur_db.wp_postmeta as asd', 'siamtur_db.wp_posts.ID', '=', 'asd.post_id')
            ->join('siamtur_db.wp_postmeta', 'siamtur_db.wp_postmeta.post_id', '=', 'asd.meta_value')
            ->where('asd.meta_key', '_thumbnail_id')
            ->where('siamtur_db.wp_postmeta.meta_key', '_wp_attached_file')
            ->where('siamtur_db.makale.tur_kod', '35')
            ->groupBy('siamtur_db.makale.id')
            ->orderBy('siamtur_db.makale.id', 'asc')
            ->select('siamtur_db.wp_posts.post_name', 'siamtur_db.wp_posts.post_title', 'asd.post_id', 'asd.meta_key', 'siamtur_db.wp_postmeta.meta_value')
            ->get();

        return view('home', ['data' => $data, 'myorum' => $myorum, 'makaleler' => $makaleUlti, 'filtre' => $deger]);

    }

    public function about()
    {
        return view('about');
    }

    public function contact()
    {
        return view('contact');
    }

    public function misafirYorumlari()
    {
        $myorum = SiamMyorum::where('kategori', 0)->orderBy('id', 'DESC')->paginate(10);
        $data = Hotel::where('status', 1)->get();
        $filtre=['5yildiz' => '0', '4yildiz' => '0', '3yildiz' => '0', 'oda_kahvalti' => '0', 'yarim_pansiyon' => '0', 'tam_pansiyon' => '0', 'hersey_dahil' => '0', 'deniz_ucagi' => '0', 'surat_motoru' => '0', 'su_ustu' => '0', 'kumsal' => '0'];

        return view('misafir_yorumlari', ['myorum' => $myorum, 'data' => $data, 'filtre' => $filtre]);
    }

    public function sendEmail(Request $request){
        $data = $request->all();
        $donus="";
        Helper::saveContactForm($data['name'], $data['phone'], $data['email'], $data['otel_secimi'], $data['message'], $data['gidis_tarihi'], $donus, $data['baslik'], $data['kisi_sayisi']);
        $dtDMY = Helper::DateConvertDMY($data['gidis_tarihi']);
        $data['gidis_tarihi'] = $dtDMY;
        $data['emails'] = ['hasan.arslanturk@siamtur.com', 'info@siamtur.com'];
        Mail::send('callusform', ['data' => $data], function ($m) use ($data) {
            $m->from('iletisim@siamtur.com', 'Maldiv Otelleri Web Sayfası!');

            $m->to($data['emails'], "Maldiv Otelleri Web Sayfası!")->subject('Maldiv Otelleri Talep Formu!');
        });
    }

    public function hotelQuery(Request $request)
    {
        $title=$_POST['query'];
        $hotel = Hotel::where('otel_ad', 'like', '%' . $title . '%')->where('destinasyon', 2)->get();
        return view('show_search_hotel')->with('sonuc', $hotel);
    }
}

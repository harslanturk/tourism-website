<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Hotel;
use App\HotelResim;
use DB;
use App\SiamMyorum;

class HotelController extends Controller
{
    public function showHotel($title, $id)
    {
    	$data = Hotel::where('id', $id)->where('destinasyon', 1)->first();
        $ic_kapak = HotelResim::where('otel_no', $data->id)->where('ic_kapak', 1)->first();
        if($ic_kapak == "")
        {
            $ic_kapak = "/images/1/Landscapes-32.jpg";
        }
        else{
            $ic_kapak = $ic_kapak->resim;
        }
    	$filtre=['5yildiz' => '0', '4yildiz' => '0', '3yildiz' => '0', 'oda_kahvalti' => '0', 'yarim_pansiyon' => '0', 'tam_pansiyon' => '0', 'hersey_dahil' => '0', 'deniz_ucagi' => '0', 'surat_motoru' => '0', 'su_ustu' => '0', 'kumsal' => '0'];
    	
    	return view('show', ['data' => $data, 'filtre' => $filtre, 'ic_kapak' => $ic_kapak]);
    }

    public function listHotel()
    {
    	$data = Hotel::where('status', 1)->where('destinasyon', 1)->get();
    	$filtre=['5yildiz' => '0', '4yildiz' => '0', '3yildiz' => '0', 'oda_kahvalti' => '0', 'yarim_pansiyon' => '0', 'tam_pansiyon' => '0', 'hersey_dahil' => '0', 'deniz_ucagi' => '0', 'surat_motoru' => '0', 'su_ustu' => '0', 'kumsal' => '0'];
    	
    	return view('all_list', ['data' => $data, 'filtre' => $filtre]);
    }

    public function listHotelfiltre(Request $request)
    {
    	$deger = $request->all();
        
    	//$filtre = "";
        if($request->has('5yildiz') != "")
        {
            $deger['5yildiz'] = 1;
            //$filtre = ['5yildiz' => '1'];
            $filtre['5yildiz'] = "1";
        }
        else
        {
            $deger['5yildiz'] = 0;
        }

        if($request->has('4yildiz') != "")
        {
            $deger['4yildiz'] = 1;
            $filtre['4yildiz'] = "1";
        }
        else
        {
            $deger['4yildiz'] = 0;
        }


        if($request->has('3yildiz') != "")
        {
            $deger['3yildiz'] = 1;
            $filtre['3yildiz'] = "1";
        }
        else
        {
            $deger['3yildiz'] = 0;
        }


        if($request->has('oda_kahvalti') != "")
        {
            $deger['oda_kahvalti'] = 1;
            $filtre['oda_kahvalti'] = "1";
        }
        else
        {
            $deger['oda_kahvalti'] = 0;
        }

        if($request->has('yarim_pansiyon') != "")
        {
            $deger['yarim_pansiyon'] = 1;
            $filtre['yarim_pansiyon'] = "1";
        }
        else
        {
            $deger['yarim_pansiyon'] = 0;
        }

        if($request->has('tam_pansiyon') != "")
        {
            $deger['tam_pansiyon'] = 1;
            $filtre['tam_pansiyon'] = "1";
        }
        else
        {
            $deger['tam_pansiyon'] = 0;
        }

        if($request->has('hersey_dahil') != "")
        {
            $deger['hersey_dahil'] = 1;
            $filtre['hersey_dahil'] = "1";
        }
        else
        {
            $deger['hersey_dahil'] = 0;
        }

        if($request->has('deniz_ucagi') != "")
        {
            $deger['deniz_ucagi'] = 1;
            $filtre['deniz_ucagi'] = "1";
        }
        else
        {
            $deger['deniz_ucagi'] = 0;
        }

        if($request->has('surat_motoru') != "")
        {
            $deger['surat_motoru'] = 1;
            $filtre['surat_motoru'] = "1";
        }
        else
        {
            $deger['surat_motoru'] = 0;
        }

        if($request->has('su_ustu') != "")
        {
            $deger['su_ustu'] = 1;
            $filtre['su_ustu'] = "1";
        }
        else
        {
            $deger['su_ustu'] = 0;
        }

        if($request->has('kumsal') != "")
        {
            $deger['kumsal'] = 1;
            $filtre['kumsal'] = "1";
        }
        else
        {
            $deger['kumsal'] = 0;
        }
        $filtre['status'] = 1;
        $filtre['destinasyon'] = 1;
        $data = Hotel::where($filtre)
    				->get();

        return view('all_list', ['data' => $data, 'filtre' => $deger]);

    }
}

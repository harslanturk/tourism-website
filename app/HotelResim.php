<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HotelResim extends Model
{
	protected $table = 'otel_foto_galeri';
	protected $fillable = [
	'otel_no','resim','kapak','ic_kapak','dis_kapak','status',
	];

}

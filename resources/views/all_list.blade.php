@extends('layouts.master')

@section('content')
<div class="hero-wrap" style="background-image: url('/images/kapak/otel1.jpg');height:420px;">
  <div class="overlay"></div>
  <div class="container">
    <div class="row no-gutters slider-text js-fullheight align-items-center justify-content-center" data-scrollax-parent="true">
      <div class="col-md-9 ftco-animate text-center" data-scrollax=" properties: { translateY: '70%' }">
        <p class="breadcrumbs" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }"><span class="mr-2"><a href="/">Anasayfa</a></span> <span class="mr-2"><a href="/">Maldivler</a></span></p>
        <h1 class="mb-3 bread" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }" style="text-shadow: rgb(0, 0, 0) 2px 2px;">Maldivler Otelleri</h1>
      </div>
    </div>
  </div>
</div>


<section class="ftco-section ftco-degree-bg">
  <div class="container">
    <div class="row">
      <div class="col-lg-3 sidebar ftco-animate">
        <div class="sidebar-wrap bg-light ftco-animate mobile-none">
          <form action="/maldiv-otelleri/filtrele" method="POST">
            {!! csrf_field() !!}
            <div class="fields">
              <h3 class="heading mb-4">Otel Ara</h3>
              <div class="form-check" style="padding-left:0; height: 43px;">
                <!--<input type="text" class="form-control notificationLink" id="notificationLink" placeholder="Otel Ara" onkeyup="getSearch()">-->
                <select class="select2-edit form-control" onkeyup="goHotel()" id="hotel_name">
                  <option></option>
                  @foreach($data as $value)
                  <option value="/<?php echo str_slug($value->otel_ad); ?>/{{$value->id}}">{{$value->otel_ad}}</option>
                  @endforeach
                </select>
                <div id="notification_li">
                    <div id="notificationContainer" style="display: none;">
                        <div id="notificationTitle">Arama Sonuçları</div>
                        <div id="notificationsBody" class="notifications">
                            <div class="notificationsImg">
                            </div>
                            <div class="notificationsText">
                            Arama yapmak için en az 3 harf giriniz.
                            </div>
                        </div>
                        <!--<div id="notificationFooter" onclick="window.location='/ara/';" style="cursor:pointer;">Tümünü Göster</div>-->
                    </div>
                </div>
              </div>
              <hr>
              <h3 class="heading mb-4">Kategori</h3>
              <div class="form-check">
                @if($filtre['5yildiz']==1)
                <input type="checkbox" class="form-check-input" checked name="5yildiz" id="check5Yildiz">
                @else
                <input type="checkbox" class="form-check-input" name="5yildiz" id="check5Yildiz">
                @endif
                <label class="form-check-label" for="check5Yildiz">
                  <p class="rate"><span><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i></span></p>
                </label>
              </div>
              <div class="form-check">
                @if($filtre['4yildiz']==1)
                <input type="checkbox" class="form-check-input" checked name="4yildiz" id="check4Yildiz">
                @else
                <input type="checkbox" class="form-check-input" name="4yildiz" id="check4Yildiz">
                @endif
                <label class="form-check-label" for="check4Yildiz">
                   <p class="rate"><span><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-o"></i></span></p>
                </label>
              </div>
              <div class="form-check">
                @if($filtre['3yildiz']==1)
                <input type="checkbox" class="form-check-input" checked name="3yildiz" id="check3Yildiz">
                @else
                <input type="checkbox" class="form-check-input" name="3yildiz" id="check3Yildiz">
                @endif
                <label class="form-check-label" for="check3Yildiz">
                  <p class="rate"><span><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-o"></i><i class="icon-star-o"></i></span></p>
               </label>
              </div>
              <hr>
              <h3 class="heading mb-4">Yemek planı</h3>
              <div class="form-check">                    
                @if($filtre['oda_kahvalti']==1)
                <input type="checkbox" class="form-check-input" checked name="oda_kahvalti" id="checkodaKahvalti">
                @else
                <input type="checkbox" class="form-check-input" name="oda_kahvalti" id="checkodaKahvalti">
                @endif
                <label class="form-check-label" for="checkodaKahvalti">
                  <p>Oda Kahvaltı</p>
               </label>
              </div>  
              <div class="form-check">
                @if($filtre['yarim_pansiyon']==1)
                <input type="checkbox" class="form-check-input" checked name="yarim_pansiyon" id="checkyarimPansiyon">
                @else
                <input type="checkbox" class="form-check-input" name="yarim_pansiyon" id="checkyarimPansiyon">
                @endif                    
                <label class="form-check-label" for="checkyarimPansiyon">
                  <p>Yarım Pansiyon</p>
               </label>
              </div>
              <div class="form-check">
                @if($filtre['tam_pansiyon']==1)
                <input type="checkbox" class="form-check-input" checked name="tam_pansiyon" id="checktamPansiyon">
                @else
                <input type="checkbox" class="form-check-input" name="tam_pansiyon" id="checktamPansiyon">
                @endif
                <label class="form-check-label" for="checktamPansiyon">
                  <p>Tam Pansiyon</p>
               </label>
              </div>
              <div class="form-check">
                @if($filtre['hersey_dahil']==1)
                <input type="checkbox" class="form-check-input" checked name="hersey_dahil" id="checkherseyDahil">
                @else
                <input type="checkbox" class="form-check-input" name="hersey_dahil" id="checkherseyDahil">
                @endif
                <label class="form-check-label" for="checkherseyDahil">
                  <p>Herşey Dahil</p>
               </label>
              </div>
              <hr>
              <h3 class="heading mb-4">Ulaşım</h3>
              <div class="form-check">
                @if($filtre['deniz_ucagi']==1)
                <input type="checkbox" class="form-check-input" checked name="deniz_ucagi" id="checkdenizUcagi">
                @else
                <input type="checkbox" class="form-check-input" name="deniz_ucagi" id="checkdenizUcagi">
                @endif
                <label class="form-check-label" for="checkdenizUcagi">
                  <p>Deniz Uçağı</p>
               </label>
              </div>
              <div class="form-check">
                @if($filtre['surat_motoru']==1)
                <input type="checkbox" class="form-check-input" checked name="surat_motoru" id="checksuratMotoru">
                @else
                <input type="checkbox" class="form-check-input" name="surat_motoru" id="checksuratMotoru">
                @endif
                <label class="form-check-label" for="checksuratMotoru">
                  <p>Sürat Motoru</p>
               </label>
              </div>
              <hr>
              <h3 class="heading mb-4">Konaklama Şekli</h3>
              <div class="form-check">
                @if($filtre['su_ustu']==1)
                <input type="checkbox" class="form-check-input" name="su_ustu" id="checksuUstu">
                @else
                <input type="checkbox" class="form-check-input" name="su_ustu" id="checksuUstu">
                @endif
                <label class="form-check-label" for="checksuUstu">
                  <p>Su Üstü</p>
               </label>
              </div>
              <div class="form-check">
                @if($filtre['kumsal']==1)
                <input type="checkbox" class="form-check-input" checked name="kumsal" id="checkkumsal">
                @else
                <input type="checkbox" class="form-check-input" name="kumsal" id="checkkumsal">
                @endif
                <label class="form-check-label" for="checkkumsal">
                  <p>Kumsal</p>
               </label>
              </div>                  
              <div class="form-group">
                <input type="submit" value="Filtrele" class="btn btn-primary py-3 px-5">
              </div>
            </div>
          </form>
        </div>
      </div>
          <div class="col-lg-9">
            <div class="row">
              @foreach($data as $value)
              <?php
              $resim = App\HotelResim::where('otel_no', $value->id)->where('dis_kapak', 1)->first();
              if($resim == "")
              {
                $resim = App\HotelResim::where('otel_no', $value->id)->first();
              }
              ?>
              <div class="col-sm col-md-6 col-lg-4 ftco-animate">
                <div class="destination">
                  <a href="/<?php echo str_slug($value->otel_ad)."/".$value->id; ?>" class="img img-2 d-flex justify-content-center align-items-center" style="background-image: url({{ $resim->resim }});">
                    <div class="icon d-flex justify-content-center align-items-center">
                      <span class="icon-search2"></span>
                    </div>
                  </a>
                  <div class="text p-3" style="min-height: 280px;">
                    <div class="d-flex">
                      <div>
                        <h3><a href="/<?php echo str_slug($value->otel_ad)."/".$value->id; ?>">{{$value->otel_ad}}</a></h3>
                        <p class="rate">
                          <span class="star">
                          @if($value['5yildiz'] == "1")
                          <i class="icon-star"></i>
                          <i class="icon-star"></i>
                          <i class="icon-star"></i>
                          <i class="icon-star"></i>
                          <i class="icon-star"></i>
                          @elseif($value['4yildiz'] == "1")
                          <i class="icon-star"></i>
                          <i class="icon-star"></i>
                          <i class="icon-star"></i>
                          <i class="icon-star"></i>
                          @elseif($value['3yildiz'] == "1")
                          <i class="icon-star"></i>
                          <i class="icon-star"></i>
                          <i class="icon-star"></i>
                          @endif
                          </span>
                        </p>
                      </div>
                    </div>
                    <p style="min-height: 95px;">{{str_limit($value->aciklama, 90)}}</p>
                    <hr>
                    <p class="bottom-area d-flex">
                      <span><i class="icon-map-o"></i> Maldivler</span> 
                      <span class="ml-auto"><a href="/<?php echo str_slug($value->otel_ad)."/".$value->id; ?>">İncele</a></span>
                    </p>
                  </div>
                </div>
              </div>
              @endforeach
              <div class="col-md-12 hotel-single ftco-animate mb-5 mt-4">
                <h4 class="mb-5">Hemen Teklif Alın!</h4>
                <div class="fields">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <input type="text" class="form-control" placeholder="Ad Soyad" id="name">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <input type="text" class="form-control" placeholder="Email" id="email">
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <input type="date" id="gidis_tarihi" class="form-control" placeholder="Gidiş Tarihi">
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <input type="number" id="kisi_sayisi" class="form-control" placeholder="Kişi Sayısı">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <input type="text" class="form-control" placeholder="Telefon" id="phone">
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <textarea class="form-control" style="height: 100px !important;" id="message" placeholder="Mesajınız"></textarea>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <input type="submit" value="Gönder" onclick="sendContact();" class="btn btn-primary py-3">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
    </div>
  </div>
</section> <!-- .section -->
@endsection
@section('edit-js')
<script>
/* Notifikasyon Ekranı */
$(document).ready(function()
{
    $(".notificationLink").click(function()
    {
   //okundu işlemi burda yapılabilir.
        $("#notificationContainer").fadeToggle(300);
        $("#notification_count").fadeOut("slow");
        return false;
    });
 //Document Click
    $(document).click(function()
    {
        $("#notificationContainer").hide();
    });
  //Popup Click
    $("#notificationContainer").click(function()
    {
        //return false
    });
});

/* Notifikasyon Ekranı */
function getSearch()
{
  var query = document.getElementById('notificationLink').value;
  var say=query.length;
  if(say>"2")
  {
  $.ajax({
    url: '/hotel-query',
    type: 'POST',
    beforeSend: function (xhr) {
                var token = $('meta[name="csrf_token"]').attr('content');

                if (token) {
                      return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                }
            },
    cache: false,
    data: {query: query},
    success: function(data){
      document.getElementById('notificationsBody').innerHTML=data;
    },
    error: function(jqXHR, textStatus, err){}
 });
  }
}
$(document).ready(function() {
    $('.select2-edit').select2({
      placeholder: "Otel Ara",
      allowClear: true
    });
});
</script>
@endsection
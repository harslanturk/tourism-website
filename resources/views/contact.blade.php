@extends('layouts.master')

@section('content')
<div class="hero-wrap" style="background-image: url('images/bg_2.jpg'); height:450px;">
  <div class="overlay"></div>
  <div class="container">
    <div class="row no-gutters slider-text js-fullheight align-items-center justify-content-center" data-scrollax-parent="true">
      <div class="col-md-9 ftco-animate text-center" data-scrollax=" properties: { translateY: '70%' }">
        <p class="breadcrumbs" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }"><span class="mr-2"><a href="/">Anasayfa</a></span> <span>İletişim</span></p>
        <h1 class="mb-3 bread" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }">İletişim</h1>
      </div>
    </div>
  </div>
</div>

<section class="ftco-section contact-section ftco-degree-bg">
  <div class="container">
    <div class="row d-flex mb-5 contact-info">
      <div class="col-md-12 mb-4">
        <h2 class="h4">İletişim Bilgilerimiz</h2>
      </div>
      <div class="w-100"></div>
      <div class="col-md-3">
        <p><span style="font-weight: bold;">Adres:</span> Bağdat Cad. Hayriye Apt. No:143 K:1 D:4 Feneryolu / Kadıköy / İstanbul</p>
      </div>
      <div class="col-md-3">
        <p><span style="font-weight: bold;">Telefon:</span> <a href="tel://02163459393">0216 345 93 93</a></p>
      </div>
      <div class="col-md-3">
        <p><span style="font-weight: bold;">E mail:</span> <a href="mailto:info@siamtur.com">info@siamtur.com</a></p>
      </div>
      <div class="col-md-3">
        <p><span style="font-weight: bold;">Web Site</span> <a href="https://siamtur.com" target="_blank">siamtur.com</a></p>
      </div>
    </div>
    <div class="row block-9">
      <div class="col-md-6 pr-md-5">
        <div class="form-group">
          <input type="text" class="form-control" id="name" placeholder="Ad Soyad">
        </div>
        <div class="form-group">
          <input type="text" class="form-control" id="email" placeholder="E Mail">
        </div>
        <div class="form-group">
          <input type="text" class="form-control" id="phone" placeholder="Telefon">
          <input type="hidden" class="form-control" id="kisi_sayisi" placeholder="Kişi Sayısı">
        </div>
        <div class="form-group">
          <input type="date" id="gidis_tarihi" class="form-control" placeholder="Gidiş Tarihi">
        </div>
        <div class="form-group">
          <textarea name="" id="message" cols="30" rows="7" class="form-control" placeholder="Mesajınız"></textarea>
        </div>
        <div class="form-group">
          <input type="hidden" id="baslik" value="Maldivlerotel.com İletişim Sayfası">
          <input type="hidden" id="otel_secimi" value="Bilgi Yok">
          <input type="submit" value="Gönder" onclick="sendContact();" class="btn btn-primary py-3 px-5">
        </div>      
      </div>

      <div class="col-md-6" id="map"></div>
    </div>
  </div>
</section>
@endsection
@section('edit-js')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAXSAqWATIA_jCGhGSwBOwqjTAi6a-pkzM"></script>
  <script src="/js/google-map.js"></script>
@endsection
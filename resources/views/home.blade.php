@extends('layouts.master')

@section('content')
    
    <div class="hero-wrap" style="background-image: url('/images/bg_1.jpg'); height:450px;">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text js-fullheight align-items-center justify-content-start" data-scrollax-parent="true">
          <div class="col-md-9 ftco-animate" data-scrollax=" properties: { translateY: '70%' }" style="text-shadow: #000 2px 2px;">
            <h1 class="mb-4" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }"><strong>Maldivler'i <br></strong> keşfedin</h1>
            <p data-scrollax="properties: { translateY: '30%', opacity: 1.6 }">Hayallerinizdeki tatile hazırmısınız?</p>
          </div>
        </div>
      </div>
    </div>

    <section class="ftco-section bg-light" style="padding-top:0;">
    	<div class="container">
				<div class="row justify-content-start mb-5 pb-3" style="margin-top: 20px; margin-bottom: 20px !important; text-align:center;">
          <div class="col-md-12 heading-section ftco-animate">
            <h1 class=""><strong>Maldivler</strong> Otelleri</h1>
          </div>
        </div>    		
    	</div>
    	<div class="container">
        <div class="row">
          <div class="col-lg-3 sidebar ftco-animate mobile-none">
            <div class="sidebar-wrap bg-light ftco-animate">
              <form action="/filtrele" method="POST">
                {!! csrf_field() !!}
                <div class="fields">
                  <h3 class="heading">Otel Ara</h3>
                  <div class="form-check" style="padding-left:0; height: 43px;">
                    <!--<input type="text" class="form-control notificationLink" id="notificationLink" placeholder="Otel Ara" onkeyup="getSearch()">-->
                    <select class="select2-edit form-control" onkeyup="goHotel()" id="hotel_name">
                      <option></option>
                      @foreach($data as $value)
                      <option value="/<?php echo str_slug($value->otel_ad); ?>/{{$value->id}}">{{$value->otel_ad}}</option>
                      @endforeach
                    </select>
                    <div id="notification_li">
                        <div id="notificationContainer" style="display: none;">
                            <div id="notificationTitle">Arama Sonuçları</div>
                            <div id="notificationsBody" class="notifications">
                                <div class="notificationsImg">
                                </div>
                                <div class="notificationsText">
                                Arama yapmak için en az 3 harf giriniz.
                                </div>
                            </div>
                            <!--<div id="notificationFooter" onclick="window.location='/ara/';" style="cursor:pointer;">Tümünü Göster</div>-->
                        </div>
                    </div>
                  </div>
                  <hr>
                  <h3 class="heading">Kategori</h3>
                  <div class="form-check">
                    @if($filtre['5yildiz']==1)
                    <input type="checkbox" class="form-check-input" checked name="5yildiz" id="check5Yildiz">
                    @else
                    <input type="checkbox" class="form-check-input" name="5yildiz" id="check5Yildiz">
                    @endif
                    <label class="form-check-label" for="check5Yildiz">
                      <p class="rate"><span><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i></span></p>
                    </label>
                  </div>
                  <div class="form-check">
                    @if($filtre['4yildiz']==1)
                    <input type="checkbox" class="form-check-input" checked name="4yildiz" id="check4Yildiz">
                    @else
                    <input type="checkbox" class="form-check-input" name="4yildiz" id="check4Yildiz">
                    @endif
                    <label class="form-check-label" for="check4Yildiz">
                       <p class="rate"><span><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-o"></i></span></p>
                    </label>
                  </div>
                  <div class="form-check">
                    @if($filtre['3yildiz']==1)
                    <input type="checkbox" class="form-check-input" checked name="3yildiz" id="check3Yildiz">
                    @else
                    <input type="checkbox" class="form-check-input" name="3yildiz" id="check3Yildiz">
                    @endif
                    <label class="form-check-label" for="check3Yildiz">
                      <p class="rate"><span><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-o"></i><i class="icon-star-o"></i></span></p>
                   </label>
                  </div>
                  <hr>
                  <h3 class="heading mb-4">Yemek planı</h3>
                  <div class="form-check">                    
                    @if($filtre['oda_kahvalti']==1)
                    <input type="checkbox" class="form-check-input" checked name="oda_kahvalti" id="checkodaKahvalti">
                    @else
                    <input type="checkbox" class="form-check-input" name="oda_kahvalti" id="checkodaKahvalti">
                    @endif
                    <label class="form-check-label" for="checkodaKahvalti">
                      <p>Oda Kahvaltı</p>
                   </label>
                  </div>  
                  <div class="form-check">
                    @if($filtre['yarim_pansiyon']==1)
                    <input type="checkbox" class="form-check-input" checked name="yarim_pansiyon" id="checkyarimPansiyon">
                    @else
                    <input type="checkbox" class="form-check-input" name="yarim_pansiyon" id="checkyarimPansiyon">
                    @endif                    
                    <label class="form-check-label" for="checkyarimPansiyon">
                      <p>Yarım Pansiyon</p>
                   </label>
                  </div>
                  <div class="form-check">
                    @if($filtre['tam_pansiyon']==1)
                    <input type="checkbox" class="form-check-input" checked name="tam_pansiyon" id="checktamPansiyon">
                    @else
                    <input type="checkbox" class="form-check-input" name="tam_pansiyon" id="checktamPansiyon">
                    @endif
                    <label class="form-check-label" for="checktamPansiyon">
                      <p>Tam Pansiyon</p>
                   </label>
                  </div>
                  <div class="form-check">
                    @if($filtre['hersey_dahil']==1)
                    <input type="checkbox" class="form-check-input" checked name="hersey_dahil" id="checkherseyDahil">
                    @else
                    <input type="checkbox" class="form-check-input" name="hersey_dahil" id="checkherseyDahil">
                    @endif
                    <label class="form-check-label" for="checkherseyDahil">
                      <p>Herşey Dahil</p>
                   </label>
                  </div>
                  <hr>
                  <h3 class="heading">Ulaşım</h3>
                  <div class="form-check">
                    @if($filtre['deniz_ucagi']==1)
                    <input type="checkbox" class="form-check-input" checked name="deniz_ucagi" id="checkdenizUcagi">
                    @else
                    <input type="checkbox" class="form-check-input" name="deniz_ucagi" id="checkdenizUcagi">
                    @endif
                    <label class="form-check-label" for="checkdenizUcagi">
                      <p>Deniz Uçağı</p>
                   </label>
                  </div>
                  <div class="form-check">
                    @if($filtre['surat_motoru']==1)
                    <input type="checkbox" class="form-check-input" checked name="surat_motoru" id="checksuratMotoru">
                    @else
                    <input type="checkbox" class="form-check-input" name="surat_motoru" id="checksuratMotoru">
                    @endif
                    <label class="form-check-label" for="checksuratMotoru">
                      <p>Sürat Motoru</p>
                   </label>
                  </div>
                  <hr>
                  <h3 class="heading">Konaklama Şekli</h3>
                  <div class="form-check">
                    @if($filtre['su_ustu']==1)
                    <input type="checkbox" class="form-check-input" name="su_ustu" id="checksuUstu">
                    @else
                    <input type="checkbox" class="form-check-input" name="su_ustu" id="checksuUstu">
                    @endif
                    <label class="form-check-label" for="checksuUstu">
                      <p>Su Üstü</p>
                   </label>
                  </div>
                  <div class="form-check">
                    @if($filtre['kumsal']==1)
                    <input type="checkbox" class="form-check-input" checked name="kumsal" id="checkkumsal">
                    @else
                    <input type="checkbox" class="form-check-input" name="kumsal" id="checkkumsal">
                    @endif
                    <label class="form-check-label" for="checkkumsal">
                      <p>Kumsal</p>
                   </label>
                  </div>                  
                  <div class="form-group" style="margin-top: 25px;">
                    <input type="submit" value="Filtrele" class="btn btn-primary py-3 px-5">
                  </div>
                </div>
              </form>
            </div>
          </div>
          <div class="col-lg-9">
        		<div class="row">
              @foreach($data as $value)
              <?php
              $resim = App\HotelResim::where('otel_no', $value->id)->where('dis_kapak', 1)->first();
              if($resim == "")
              {
                $resim = App\HotelResim::where('otel_no', $value->id)->first();
              }
              ?>
        			<div class="col-sm col-md-6 col-lg-4 ftco-animate">
        				<div class="destination">
        					<a href="/<?php echo str_slug($value->otel_ad)."/".$value->id; ?>" class="img img-2 d-flex justify-content-center align-items-center" style="background-image: url({{ $resim->resim }});">
        						<div class="icon d-flex justify-content-center align-items-center">
        							<span class="icon-search2"></span>
        						</div>
        					</a>
        					<div class="text p-3" style="min-height: 280px;">
        						<div class="d-flex">
        							<div>
    		    						<h3><a href="/<?php echo str_slug($value->otel_ad)."/".$value->id; ?>">{{$value->otel_ad}}</a></h3>
    		    						<p class="rate">
    		    							<span class="star">
                          @if($value['5yildiz'] == "1")
                          <i class="icon-star"></i>
                          <i class="icon-star"></i>
                          <i class="icon-star"></i>
                          <i class="icon-star"></i>
                          <i class="icon-star"></i>
                          @elseif($value['4yildiz'] == "1")
                          <i class="icon-star"></i>
                          <i class="icon-star"></i>
                          <i class="icon-star"></i>
                          <i class="icon-star"></i>
                          @elseif($value['3yildiz'] == "1")
                          <i class="icon-star"></i>
                          <i class="icon-star"></i>
                          <i class="icon-star"></i>
                          @endif
                          </span>
    		    						</p>
    	    						</div>
        						</div>
        						<p style="min-height: 95px;">{{str_limit($value->aciklama, 90)}}</p>
        						<hr>
        						<p class="bottom-area d-flex">
        							<span><i class="icon-map-o"></i> Maldivler</span> 
        							<span class="ml-auto"><a href="/<?php echo str_slug($value->otel_ad)."/".$value->id; ?>">İncele</a></span>
        						</p>
        					</div>
        				</div>
        			</div>
              @endforeach
        		</div>
          </div>
        </div>
    	</div>
    </section>

    <section class="ftco-section ftco-counter img" id="section-counter" style="background-image: url(/images/bg_1.jpg);">
    	<div class="container">
    		<div class="row justify-content-center mb-5 pb-3">
          <div class="col-md-7 text-center heading-section heading-section-white ftco-animate">
            <h2 class="mb-4">Maldivler sizi bekliyor!</h2>
            <h3 class="subheading">Rüya gibi bir balayı için Maldivler hazır, peki ya siz?</h3>
          </div>
        </div>
    	</div>
    </section>

    <section class="ftco-section testimony-section bg-light">
      <div class="container">
        <div class="row justify-content-start">
          <div class="col-md-5 heading-section ftco-animate">
          	<span class="subheading">Her Şey Size Özel!</span>
            <h2 class="mb-4 pb-3"><strong>Neden</strong> Bizi Seçmelisiniz?</h2>
            <p>Uzak doğu ve Hint Okyanusu destinasyonlarımızda yüzde yüz müşteri memnuniyeti öncelikli çalışma prensibimizle her sene yüzlerce balayı çifti bu özel tatillerinde bizi seçmektedir. Aracı kullanmadan direkt kontratlarla çalıştığımız Maldivler, Phuket, Samui, Zanzibar, Boracay, Mauritius ve Seyşeller başta olmak üzere birçok balayı cenneti destinasyonda en uygun fiyatlarla balayınızı organize edebilmemiz için bizi arayın!</p>
            <p><a href="#" class="btn btn-primary btn-outline-primary mt-4 px-4 py-3">Devam</a></p>
          </div>
					<div class="col-md-1"></div>
          <div class="col-md-6 heading-section ftco-animate">
          	<span class="subheading">Sizin Mutluluğunuz, Bizim Referansımız!</span>
            <h2 class="mb-4 pb-3"><strong>Müşteri</strong> Yorumları</h2>
          	<div class="row ftco-animate">
		          <div class="col-md-12">
		            <div class="carousel-testimony owl-carousel">
                  @foreach($myorum as $value)
		              <div class="item">
		                <div class="testimony-wrap d-flex" style="min-height:330px;">
		                  <div class="user-img" style="width:100%;position: absolute; top:0; left:0;">
		                    <span class="quote d-flex align-items-center justify-content-center" style="left:10px; top:10px;">
		                      <i class="icon-quote-left"></i>
		                    </span>
		                  </div>
		                  <div class="text ml-md-4" style="width:100%;">
		                    <p class="mb-5" style="margin-bottom: 1rem !important;"><?php echo substr($value->mesaj,0,400)."..."; ?></p>
		                    <p class="name">{{ $value->gonderen }}</p>
		                    <span class="position">{{ $value->tur }}</span>
		                  </div>
                      <div class="user-img" style="width:100%;position: absolute; bottom:0; right:0;">
                        <span class="quote d-flex align-items-center justify-content-center" style="right:10px; bottom:10px;">
                          <i class="icon-quote-right"></i>
                        </span>
                      </div>
		                </div>
		              </div>
                  @endforeach		              
		            </div>
		          </div>
		        </div>
          </div>
        </div>
      </div>
    </section>

    <section class="ftco-section bg-light">
      <div class="container">
        <div class="row justify-content-start mb-5 pb-3">
          <div class="col-md-7 heading-section ftco-animate">
            <span class="subheading">En Son Eklenenler</span>
            <h2><strong>Maldivler</strong> &amp; Yazılarımız</h2>
          </div>
        </div>
        <div class="row d-flex">
          @foreach($makaleler as $makale)
          <div class="col-md-3 d-flex ftco-animate">
            <div class="blog-entry align-self-stretch">
              <a href="https://siamtur.com/uzakdogugazetesi/{{$makale->post_name}}" target="_blank" class="block-20" style="background-image: url('https://siamtur.com/uzakdogugazetesi/wp-content/uploads/{{$makale->meta_value}}');">
              </a>
              <div class="text p-4 d-block">
              	<span class="tag">Maldivler, Balayı</span>
                <h3 class="heading mt-3"><a href="https://siamtur.com/uzakdogugazetesi/{{$makale->post_name}}" target="_blank">{{$makale->post_title}}</a></h3>                
              </div>
            </div>
          </div>
          @endforeach
        </div>
      </div>
    </section>
		
		<section class="ftco-section-parallax">
      <div class="parallax-img d-flex align-items-center">
        <div class="container">
          <div class="row d-flex justify-content-center">
            <div class="col-md-7 text-center heading-section heading-section-white ftco-animate">
              <h2>E-Mail Listemize Kaydolun!</h2>
              <p>Maldivler destinasyonumuzda kampanya ve fırsatlarımızdan ilk siz haberdar olmak istiyorsanız, e-mail listemize kaydolun. İlk sizin haberiniz olsun!</p>
              <div class="row d-flex justify-content-center mt-5">
                <div class="col-md-8">
                  <form action="#" class="subscribe-form">
                    <div class="form-group d-flex">
                      <input type="text" class="form-control" placeholder="E-Mail Adresiniz">
                      <input type="submit" value="Gönder" class="submit px-3">
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

@endsection
@section('edit-js')
<script>
/* Notifikasyon Ekranı */
$(document).ready(function()
{
    $(".notificationLink").click(function()
    {
   //okundu işlemi burda yapılabilir.
        $("#notificationContainer").fadeToggle(300);
        $("#notification_count").fadeOut("slow");
        return false;
    });
 //Document Click
    $(document).click(function()
    {
        $("#notificationContainer").hide();
    });
  //Popup Click
    $("#notificationContainer").click(function()
    {
        //return false
    });
});

/* Notifikasyon Ekranı */
function getSearch()
{
  var query = document.getElementById('notificationLink').value;
  var say=query.length;
  if(say>"2")
  {
  $.ajax({
    url: '/hotel-query',
    type: 'POST',
    beforeSend: function (xhr) {
                var token = $('meta[name="csrf_token"]').attr('content');

                if (token) {
                      return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                }
            },
    cache: false,
    data: {query: query},
    success: function(data){
      document.getElementById('notificationsBody').innerHTML=data;
    },
    error: function(jqXHR, textStatus, err){}
 });
  }
}
$(document).ready(function() {
    $('.select2-edit').select2({
      placeholder: "Otel Ara",
      allowClear: true
    });
});
</script>
@endsection
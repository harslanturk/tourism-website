@extends('layouts.master')

@section('content')
<div class="hero-wrap" style="background-image: url('{{$ic_kapak}}');height:420px;">
  <div class="overlay"></div>
  <div class="container">
    <div class="row no-gutters slider-text align-items-center justify-content-center" data-scrollax-parent="true" style="padding-top: 220px;"><!--js-fullheight-->
      <div class="col-md-9 ftco-animate text-center" data-scrollax=" properties: { translateY: '70%' }">
        <p class="breadcrumbs" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }"><span class="mr-2"><a href="/">Anasayfa</a></span> <span class="mr-2"><a href="/">Maldivler</a></span> <span><a href="/<?php echo str_slug($data->otel_ad)."/".$data->id; ?>">{{$data->otel_ad}}</a></span></p>
        <h1 class="mb-3 bread" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }" style="text-shadow: rgb(0, 0, 0) 2px 2px;">{{$data->otel_ad}}</h1>
      </div>
    </div>
  </div>
</div>


<section class="ftco-section ftco-degree-bg">
  <div class="container">
    <div class="row">
      <div class="col-lg-3 sidebar ftco-animate" style="display:none;">
        <div class="sidebar-wrap bg-light ftco-animate">
          <h3 class="heading mb-4">Kategori</h3>
          <form action="/filtrele" method="POST">
            {!! csrf_field() !!}
            <div class="fields">
              <div class="form-check">
                @if($filtre['5yildiz']==1)
                <input type="checkbox" class="form-check-input" checked name="5yildiz" id="check5Yildiz">
                @else
                <input type="checkbox" class="form-check-input" name="5yildiz" id="check5Yildiz">
                @endif
                <label class="form-check-label" for="check5Yildiz">
                  <p class="rate"><span><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i></span></p>
                </label>
              </div>
              <div class="form-check">
                @if($filtre['4yildiz']==1)
                <input type="checkbox" class="form-check-input" checked name="4yildiz" id="check4Yildiz">
                @else
                <input type="checkbox" class="form-check-input" name="4yildiz" id="check4Yildiz">
                @endif
                <label class="form-check-label" for="check4Yildiz">
                   <p class="rate"><span><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-o"></i></span></p>
                </label>
              </div>
              <!--<div class="form-check">
                @if($filtre['3yildiz']==1)
                <input type="checkbox" class="form-check-input" checked name="3yildiz" id="check3Yildiz">
                @else
                <input type="checkbox" class="form-check-input" name="3yildiz" id="check3Yildiz">
                @endif
                <label class="form-check-label" for="check3Yildiz">
                  <p class="rate"><span><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-o"></i><i class="icon-star-o"></i></span></p>
               </label>
              </div>-->
              <hr>
              <h3 class="heading mb-4">Yemek planı</h3>
              <div class="form-check">                    
                @if($filtre['oda_kahvalti']==1)
                <input type="checkbox" class="form-check-input" checked name="oda_kahvalti" id="checkodaKahvalti">
                @else
                <input type="checkbox" class="form-check-input" name="oda_kahvalti" id="checkodaKahvalti">
                @endif
                <label class="form-check-label" for="checkodaKahvalti">
                  <p>Oda Kahvaltı</p>
               </label>
              </div>  
              <div class="form-check">
                @if($filtre['yarim_pansiyon']==1)
                <input type="checkbox" class="form-check-input" checked name="yarim_pansiyon" id="checkyarimPansiyon">
                @else
                <input type="checkbox" class="form-check-input" name="yarim_pansiyon" id="checkyarimPansiyon">
                @endif                    
                <label class="form-check-label" for="checkyarimPansiyon">
                  <p>Yarım Pansiyon</p>
               </label>
              </div>
              <div class="form-check">
                @if($filtre['tam_pansiyon']==1)
                <input type="checkbox" class="form-check-input" checked name="tam_pansiyon" id="checktamPansiyon">
                @else
                <input type="checkbox" class="form-check-input" name="tam_pansiyon" id="checktamPansiyon">
                @endif
                <label class="form-check-label" for="checktamPansiyon">
                  <p>Tam Pansiyon</p>
               </label>
              </div>
              <div class="form-check">
                @if($filtre['hersey_dahil']==1)
                <input type="checkbox" class="form-check-input" checked name="hersey_dahil" id="checkherseyDahil">
                @else
                <input type="checkbox" class="form-check-input" name="hersey_dahil" id="checkherseyDahil">
                @endif
                <label class="form-check-label" for="checkherseyDahil">
                  <p>Herşey Dahil</p>
               </label>
              </div>
              <hr>
              <h3 class="heading mb-4">Ulaşım</h3>
              <div class="form-check">
                @if($filtre['deniz_ucagi']==1)
                <input type="checkbox" class="form-check-input" checked name="deniz_ucagi" id="checkdenizUcagi">
                @else
                <input type="checkbox" class="form-check-input" name="deniz_ucagi" id="checkdenizUcagi">
                @endif
                <label class="form-check-label" for="checkdenizUcagi">
                  <p>Deniz Uçağı</p>
               </label>
              </div>
              <div class="form-check">
                @if($filtre['surat_motoru']==1)
                <input type="checkbox" class="form-check-input" checked name="surat_motoru" id="checksuratMotoru">
                @else
                <input type="checkbox" class="form-check-input" name="surat_motoru" id="checksuratMotoru">
                @endif
                <label class="form-check-label" for="checksuratMotoru">
                  <p>Sürat Motoru</p>
               </label>
              </div>
              <hr>
              <h3 class="heading mb-4">Konaklama Şekli</h3>
              <div class="form-check">
                @if($filtre['su_ustu']==1)
                <input type="checkbox" class="form-check-input" name="su_ustu" id="checksuUstu">
                @else
                <input type="checkbox" class="form-check-input" name="su_ustu" id="checksuUstu">
                @endif
                <label class="form-check-label" for="checksuUstu">
                  <p>Su Üstü</p>
               </label>
              </div>
              <div class="form-check">
                @if($filtre['kumsal']==1)
                <input type="checkbox" class="form-check-input" checked name="kumsal" id="checkkumsal">
                @else
                <input type="checkbox" class="form-check-input" name="kumsal" id="checkkumsal">
                @endif
                <label class="form-check-label" for="checkkumsal">
                  <p>Kumsal</p>
               </label>
              </div>                  
              <div class="form-group">
                <input type="submit" value="Filtrele" class="btn btn-primary py-3 px-5">
              </div>
            </div>
          </form>
        </div>
      </div>
      <div class="col-lg-12">
        <div class="row">
          <div class="col-md-12 hotel-single mt-4 mb-5 ftco-animate">
            <span>Maldivler</span>
            <h2>{{$data->otel_ad}}</h2>
            <p class="rate mb-5">
              <span class="loc"><a href="#"><i class="icon-map"></i> <?php echo $data->adres; ?></a></span>
              <span class="star">
              @if($data['5yildiz'] == "1")
              <i class="icon-star"></i>
              <i class="icon-star"></i>
              <i class="icon-star"></i>
              <i class="icon-star"></i>
              <i class="icon-star"></i>
              @elseif($data['4yildiz'] == "1")
              <i class="icon-star"></i>
              <i class="icon-star"></i>
              <i class="icon-star"></i>
              <i class="icon-star"></i>
              @elseif($data['3yildiz'] == "1")
              <i class="icon-star"></i>
              <i class="icon-star"></i>
              <i class="icon-star"></i>
              @endif
              </span>
            </p>
            <p>{{$data->aciklama}}</p>
            <h4>Konum:</h4>
            <p>{{$data->konum}}</p>            
            <h4>Otel Olanakları:</h4>
            <p>{{$data->olanaklar}}</p>
          </div>
          <div class="col-md-12 ftco-animate">
            <div class="single-slider owl-carousel">
              <?php
                $hotelResimler = App\HotelResim::where('otel_no', $data->id)->get();
              ?>
              @foreach($hotelResimler as $otel)
              @if($otel->ic_kapak != 1 && $otel->dis_kapak != 1)
              <div class="item">
                <a href="{{$otel->resim}}" data-lightbox="maldivler" alt="{{$data->otel_ad}}"><div class="hotel-img" style="background-image: url({{$otel->resim}});"></div></a>
              </div>
              @endif
              @endforeach              
            </div>
          </div>
          @if($data->youtube != "")
          <div class="col-md-12 hotel-single ftco-animate mb-5 mt-4">
            <h3 class="mb-4">{{$data->otel_ad}} Tanıtım Videosu</h3>
            <div class="block-16" style="max-width: 560px;">
              <iframe width="100%" height="315" src="https://www.youtube.com/embed/{{$data->youtube}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
          </div>
          @endif
          <div class="col-md-12 hotel-single ftco-animate mb-5 mt-4">
            <h4 class="mb-5">Hemen Teklif Alın!</h4>
            <div class="fields">
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group contact-label">
                    <label>Otel Seçiminiz</label>
                  </div>
                  <div class="form-group">
                    <select class="form-control" id="otel_secimi">
                      <option>Sadece bu otel için teklif istiyorum</option>
                      <option>Bu otelle birlikte önereceğiniz tüm oteller için teklif almak istiyorum.</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group contact-label">
                    <label>Ad Soyad</label>
                  </div>
                  <div class="form-group">
                    <input type="text" class="form-control" placeholder="Ad Soyad" id="name">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group contact-label">
                    <label>E-Mail</label>
                  </div>
                  <div class="form-group">
                    <input type="text" class="form-control" placeholder="Email" id="email">
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group contact-label">
                    <label>Seyahat Başlangıç Tarihi</label>
                  </div>
                  <div class="form-group">
                    <input type="date" id="gidis_tarihi" class="form-control" placeholder="Gidiş Tarihi"><!--checkin_date-->
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group contact-label">
                    <label>Kişi Sayısı</label>
                  </div>
                  <div class="form-group contact-label">
                    <input type="number" id="kisi_sayisi" class="form-control" placeholder="Kişi Sayısı">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group contact-label">
                    <label>Telefon</label>
                  </div>
                  <div class="form-group">
                    <input type="text" class="form-control" placeholder="Telefon" id="phone">
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group contact-label">
                    <label>Standart olarak 5 gece otel konaklamalı 7 günlük tur paketi şeklinde teklif hazırlanmaktadır. Bunun dışında bir tercihiniz varsa lütfen mesaj bölümüne yazınız.</label>
                  </div>
                  <div class="form-group">
                    <textarea class="form-control" style="height: 100px !important;" id="message" placeholder="Mesajınız"></textarea>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <input type="hidden" id="baslik" value="{{$data->otel_ad}}">
                    <input type="submit" value="Gönder" onclick="sendContact();" class="btn btn-primary py-3">
                  </div>
                </div>
                <div class="col-md-12">
                  *Yukarıda verilen bilgiler otellerden temin edilmiş olup, oteller bilgilerini haber vermeksizin değiştirme haklarını saklı tutmaktadır.
                </div>
              </div>
            </div>
          </div>
          <!--<div class="col-md-12 hotel-single ftco-animate mb-5 mt-5">
            <h4 class="mb-4">Related Hotels</h4>
            <div class="row">
              <div class="col-md-4">
                <div class="destination">
                  <a href="hotel-single.html" class="img img-2" style="background-image: url(/images/hotel-1.jpg);"></a>
                  <div class="text p-3">
                    <div class="d-flex">
                      <div class="one">
                        <h3><a href="hotel-single.html">Hotel, Italy</a></h3>
                        <p class="rate">
                          <i class="icon-star"></i>
                          <i class="icon-star"></i>
                          <i class="icon-star"></i>
                          <i class="icon-star"></i>
                          <i class="icon-star-o"></i>
                          <span>8 Rating</span>
                        </p>
                      </div>
                      <div class="two">
                        <span class="price per-price">$40<br><small>/night</small></span>
                      </div>
                    </div>
                    <p>Far far away, behind the word mountains, far from the countries</p>
                    <hr>
                    <p class="bottom-area d-flex">
                      <span><i class="icon-map-o"></i> Miami, Fl</span> 
                      <span class="ml-auto"><a href="#">Book Now</a></span>
                    </p>
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="destination">
                  <a href="hotel-single.html" class="img img-2" style="background-image: url(/images/hotel-2.jpg);"></a>
                  <div class="text p-3">
                    <div class="d-flex">
                      <div class="one">
                        <h3><a href="hotel-single.html">Hotel, Italy</a></h3>
                        <p class="rate">
                          <i class="icon-star"></i>
                          <i class="icon-star"></i>
                          <i class="icon-star"></i>
                          <i class="icon-star"></i>
                          <i class="icon-star-o"></i>
                          <span>8 Rating</span>
                        </p>
                      </div>
                      <div class="two">
                        <span class="price per-price">$40<br><small>/night</small></span>
                      </div>
                    </div>
                    <p>Far far away, behind the word mountains, far from the countries</p>
                    <hr>
                    <p class="bottom-area d-flex">
                      <span><i class="icon-map-o"></i> Miami, Fl</span> 
                      <span class="ml-auto"><a href="#">Book Now</a></span>
                    </p>
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="destination">
                  <a href="hotel-single.html" class="img img-2" style="background-image: url(/images/hotel-3.jpg);"></a>
                  <div class="text p-3">
                    <div class="d-flex">
                      <div class="one">
                        <h3><a href="hotel-single.html">Hotel, Italy</a></h3>
                        <p class="rate">
                          <i class="icon-star"></i>
                          <i class="icon-star"></i>
                          <i class="icon-star"></i>
                          <i class="icon-star"></i>
                          <i class="icon-star-o"></i>
                          <span>8 Rating</span>
                        </p>
                      </div>
                      <div class="two">
                        <span class="price per-price">$40<br><small>/night</small></span>
                      </div>
                    </div>
                    <p>Far far away, behind the word mountains, far from the countries</p>
                    <hr>
                    <p class="bottom-area d-flex">
                      <span><i class="icon-map-o"></i> Miami, Fl</span> 
                      <span class="ml-auto"><a href="#">Book Now</a></span>
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>-->

        </div>
      </div> <!-- .col-md-8 -->
    </div>
  </div>
</section> <!-- .section -->
@endsection
@section('edit-js')
@endsection
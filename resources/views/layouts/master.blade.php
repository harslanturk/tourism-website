<!DOCTYPE html>
<html lang="tr">
  <head>
    <title>Maldiv Otelleri</title>
    <meta charset="utf-8">
    <meta name="Language" content="Turkish">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf_token" content="{{ csrf_token() }}" />
    <link rel="apple-touch-icon" sizes="57x57" href="/favicons/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/favicons/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/favicons/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/favicons/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/favicons/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/favicons/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/favicons/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/favicons/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/favicons/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/favicons/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicons/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicons/favicon-16x16.png">
    <link rel="manifest" href="/favicons/manifest.json">
    <meta name="msapplication-TileImage" content="/favicons/ms-icon-144x144.png">
    
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Alex+Brush" rel="stylesheet">

    <link rel="stylesheet" href="/css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="/css/animate.css">
    
    <link rel="stylesheet" href="/css/owl.carousel.min.css">
    <link rel="stylesheet" href="/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="/css/magnific-popup.css">

    <link rel="stylesheet" href="/css/aos.css">

    <link rel="stylesheet" href="/css/ionicons.min.css">

    <link rel="stylesheet" href="/css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="/css/jquery.timepicker.css">

    <link href="/css/lightbox.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

    <link rel="stylesheet" href="/css/flaticon.css">
    <link rel="stylesheet" href="/css/icomoon.css">
    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="/css/edit.css">
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-69799470-2"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      gtag('config', 'AW-1037744117');
      gtag('config', 'UA-69799470-2');
    </script>
  </head>
  <body>
    
  <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
    <div class="container">
      <a class="navbar-brand" href="/">Maldivler Otel</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="oi oi-menu"></span> Menu
      </button>

      <div class="collapse navbar-collapse" id="ftco-nav">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item"><a href="/" class="nav-link">Anasayfa</a></li>
          <li class="nav-item"><a href="/hakkimizda" class="nav-link">Hakkımızda</a></li>
          <li class="nav-item"><a href="/maldiv-otelleri" class="nav-link">Maldivler Otelleri</a></li>
          <li class="nav-item"><a href="/misafir-yorumlari" class="nav-link">Misafir Yorumları</a></li>
          <li class="nav-item"><a href="http://www.siamtur.com/uzakdogugazetesi/" target="_blank" class="nav-link">Blog</a></li>
          <li class="nav-item"><a href="/iletisim" class="nav-link">İletişim</a></li>
          <li class="nav-item cta"><a href="/iletisim" class="nav-link"><span>Teklif Al</span></a></li>
        </ul>
      </div>
    </div>
  </nav>
    <!-- END nav -->
    
    @yield('content')
    <input type="button" id="denemetrig" onclick="gtag_report_conversion()" style="display: none;">
    <footer class="ftco-footer ftco-bg-dark ftco-section">
      <div class="container">
        <div class="row mb-5">
          <div class="col-md">
            <div class="ftco-footer-widget mb-4">
              <h2 class="ftco-heading-2">Maldiv Otelleri</h2>
              <p>Siamtur olarak Uzak Doğu ve Hint Okyanusu’nda bölgesel uzmanlık prensibiyle çalışmaktayız.</p>
              <ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-5">
                <li class="ftco-animate"><a href="https://twitter.com/siamtur"><span class="icon-twitter"></span></a></li>
                <li class="ftco-animate"><a href="https://www.facebook.com/siamtur"><span class="icon-facebook"></span></a></li>
                <li class="ftco-animate"><a href="https://www.instagram.com/siamturizm/"><span class="icon-instagram"></span></a></li>
              </ul>
            </div>
          </div>
          <div class="col-md">
            <div class="ftco-footer-widget mb-4 ml-md-5">
              <h2 class="ftco-heading-2">Popüler Otellerimiz</h2>
              <ul class="list-unstyled">
                <li><a href="/meeru-island-resort-spa/3" class="py-2 d-block">Meeru Island Resort & Spa</a></li>
                <li><a href="/anantara-kihavah-villas/5" class="py-2 d-block">Anantara Kihavah Villas</a></li>
                <li><a href="/anantara-veli-maldives-resort/6" class="py-2 d-block">Anantara Veli Maldives Resort</a></li>
                <li><a href="/lux-south-ari-atoll/8" class="py-2 d-block">LUX* South Ari Atoll</a></li>
                <li><a href="/coco-palm-bodu-hithi/9" class="py-2 d-block">Coco Palm Bodu Hithi</a></li>
                <li><a href="/anantara-dhigu-maldives-resort/7" class="py-2 d-block">Anantara Dhigu Maldives Resort</a></li>
              </ul>
            </div>
          </div>
          <div class="col-md">
             <div class="ftco-footer-widget mb-4">
              <h2 class="ftco-heading-2">Hızlı Menü</h2>
              <ul class="list-unstyled">
                <li><a href="/anasayfa" class="py-2 d-block">Anasayfa</a></li>
                <li><a href="/hakkimizda" class="py-2 d-block">Hakkımızda</a></li>
                <li><a href="/maldiv-otelleri" class="py-2 d-block">Maldiv Otelleri</a></li>
                <li><a href="/blog" class="py-2 d-block">Blog</a></li>
                <li><a href="/iletisim" class="py-2 d-block">İletişim</a></li>
              </ul>
            </div>
          </div>
          <div class="col-md">
            <div class="ftco-footer-widget mb-4">
            	<h2 class="ftco-heading-2">Bize Ulaşın</h2>
            	<div class="block-23 mb-3">
	              <ul>
	                <li><span class="icon icon-map-marker"></span><span class="text">Bağdat Cad. Hayriye Apt. No:143 K:1 D:4 Feneryolu / Kadıköy / İstanbul</span></li>
	                <li><a href="tel:4443695"><span class="icon icon-phone"></span><span class="text">444 3 695</span></a></li>
	                <li><a href="mailto:info@siamtur.com"><span class="icon icon-envelope"></span><span class="text">info@siamtur.com</span></a></li>
	              </ul>
	            </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 text-center">

            <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
  Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | <i class="icon-heart" aria-hidden="true"></i> by <a href="https://siamtur.com" target="_blank">Siamtur.com</a>
  <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
          </div>
        </div>
      </div>
    </footer>
    
  

  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


  <script src="/js/jquery.min.js"></script>
  <script src="/js/jquery-migrate-3.0.1.min.js"></script>
  <script src="/js/popper.min.js"></script>
  <script src="/js/bootstrap.min.js"></script>
  <script src="/js/jquery.easing.1.3.js"></script>
  <script src="/js/jquery.waypoints.min.js"></script>
  <script src="/js/jquery.stellar.min.js"></script>
  <script src="/js/owl.carousel.min.js"></script>
  <script src="/js/jquery.magnific-popup.min.js"></script>
  <script src="/js/aos.js"></script>
  <script src="/js/jquery.animateNumber.min.js"></script>
  <script src="/js/bootstrap-datepicker.js"></script>
  <!--<script src="/js/jquery.timepicker.min.js"></script>-->
  <script src="/js/scrollax.min.js"></script>
  <script src="/js/lightbox.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
  <script src="/js/main.js"></script>
  <script>
    function sendContact() {
      var otel_secimi = document.getElementById("otel_secimi").value;
      var baslik = document.getElementById("baslik").value;
      var name = document.getElementById("name").value;
      var email = document.getElementById("email").value;
      var phone = document.getElementById("phone").value;
      var gidis_tarihi = document.getElementById("gidis_tarihi").value;
      var kisi_sayisi = document.getElementById("kisi_sayisi").value;
      var message = document.getElementById("message").value;
      if(name == "")
      {
          alert("Lütfen 'Adınız' alanını doldurunuz.");
      }
      else if(email == "" && phone == "")
      {
          alert("Lütfen 'E-Mail' veya 'Telefon' alanlarından birini doldurunuz.");
      }
      else
      {
          $.ajax({
              url: '/send-email',
              type: 'POST',
              beforeSend: function (xhr) {
                  var token = $('meta[name="csrf_token"]').attr('content');

                  if (token) {
                      return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                  }
              },
              cache: false,
              data: {otel_secimi: otel_secimi, baslik: baslik, name: name, email: email, message: message, phone: phone, gidis_tarihi: gidis_tarihi, kisi_sayisi: kisi_sayisi},
              success: function(data){
                  /*location.href = "/";*/
                  $("#denemetrig").click();
                  alert("Bilgileriniz bize ulaşmıştır. En kısa sürede size dönüş sağlayacağız.")
                  location.reload();
              },
              error: function(jqXHR, textStatus, err){}
          });
      }
    }

    function goHotel() {
      var otel_secimi = document.getElementById("hotel_name").value;
      location.href = otel_secimi;          
    }

    $(function(){
      $('.select2-edit').on('change', function() {
        var data = $(".select2-edit option:selected").val();
        window.location = data;
      })
    });

    /*$('#hotel_name').on("select2:selecting", function(e) { 
       var otel_secimi = $("#hotel_name").val();
       console.log(otel_secimi);
       var otel_hakkimizda = "/hakkimizda";
      //location.href = "http://maldivoteller.localhost"+otel_secimi;
      //window.location = otel_hakkimizda;
      //window.location.href = "http://maldivoteller.localhost"+otel_secimi;
    });*/

  </script>
<!-- Event snippet for Tüm Formlar conversion page
In your html page, add the snippet and call gtag_report_conversion when someone clicks on the chosen link or button. -->
<script>
function gtag_report_conversion(url) {
  var callback = function () {
    if (typeof(url) != 'undefined') {
      window.location = url;
    }
  };
  gtag('event', 'conversion', {
      'send_to': 'AW-1037744117/UsqrCOzQwJQBEPXv6u4D',
      'event_callback': callback
  });
  return false;
}
</script>
  @yield('edit-js')
  </body>
</html>
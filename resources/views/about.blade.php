@extends('layouts.master')

@section('content')
<div class="hero-wrap" style="background-image: url('images/bg_2.jpg'); height:450px;">
  <div class="overlay"></div>
  <div class="container">
    <div class="row no-gutters slider-text js-fullheight align-items-center justify-content-center" data-scrollax-parent="true">
      <div class="col-md-9 text-center ftco-animate" data-scrollax=" properties: { translateY: '70%' }">
        <p class="breadcrumbs" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }"><span class="mr-2"><a href="/">Anasayfa</a></span> <span>Hakkımızda</span></p>
        <h1 class="mb-3 bread" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }">Hakkımızda</h1>
      </div>
    </div>
  </div>
</div>

<section class="ftco-section">
  <div class="container">
    <div class="row d-md-flex">
      <div class="col-md-12 ftco-animate p-md-5">
        <div class="row">
          <div class="col-md-12 d-flex align-items-center">
            
            <div class="tab-content ftco-animate" id="v-pills-tabContent">

              <div class="tab-pane fade show active" id="v-pills-whatwedo" role="tabpanel" aria-labelledby="v-pills-whatwedo-tab">
                <div class="row justify-content-start">
                  <div class="col-md-7 heading-section ftco-animate">
                    <span class="subheading">BİZ KİMİZ?</span>
                    <h2 class="mb-4"><strong>Hakkımızda</strong></h2>
                  </div>
                </div>
                <p class="moryazi"><br />Siamtur olarak Uzak Doğu ve Hint Okyanusu’nda bölgesel uzmanlık prensibiyle çalışmaktayız. Merkezimiz Kadıköy Bağdat Caddesi 143/4 adresinde bulunmaktadır.
                </p>
                <p class="moryazi"><br />
                    Qatar Havayolları tarafından beş sene üst üste Türkiye’deki en büyük 10 satışçı operatörden biri unvanını aldık. Maldivler, Phuket, Bali gibi balayı destinasyonlarında birçok direkt otel anlaşmamız sayesinde aracı kullanmadan direkt paket fiyat avantajı sağlamaktayız. Vietnam, Kamboçya, Laos, Sri Lanka, Filipinler, Myanmar destinasyonlarında uzun yıllardır yaptığımız aylık grup seferleriyle sektör öncülerinden biri konumundayız.
                </p>
                <p class="moryazi"><br />
                    1987 yılından beri turizmin her alanında hizmet vermekte olan Olcartur Şirketler Grubu'nun bir parçasıyız.
                </p>
                <p class="moryazi"><br />
                    Rehberlerimiz, bölgeyi en iyi bilen Siamtur çalışanlarından oluşmaktadır ve bu sebepten firma prestijimiz ve servis kalitemiz tur esnasında sürekli kontrol edilmektedir.
                </p>
                <p class="moryazi"><br />
                    Turumuzdaki oteller, transfer araçları ve havayolu firmaları uzun yıllarda edindiğimiz tecrübeyle özenle seçilmektedir.
                </p>
                <p class="moryazi"><br />
                    Yolcularımızın rahatı için maksimum kültürel alanı kapsamakla beraber, minimum yoruculukta dakika dakika özenle programlama yapılmıştır.
                </p>
                <p class="moryazi"><br />
                    Operatör olan firmamız direkt olarak yerel sağlayıcılarla çalışmaktadır. Büyük volümlü çalışmalarımızın da etkisiyle bölgenin en iyi fiyatları bizde bulunmaktadır.
                </p>
                <p class="moryazi">
                    Siamtur Turizm Ltd Şti,  Travel with Siam adıyla TÜRSAB’a kayıtlı A grubu seyahat acentasıdır. Belge No: 7794.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="ftco-section bg-light">
  <div class="container">
    <div class="row justify-content-start mb-5 pb-3">
      <div class="col-md-7 heading-section ftco-animate">
        <span class="subheading">S.S.S.</span>
        <h2 class="mb-4"><strong>Sık Sorulan</strong> Sorular</h2>
      </div>
    </div>  
    <div class="row">
      <div class="col-md-12 ftco-animate">
        <div id="accordion">
          <div class="row">
            <div class="col-md-6">
              <div class="card">
                <div class="card-header">
                  <a class="card-link" data-toggle="collapse"  href="#menuone" aria-expanded="true" aria-controls="menuone">Ödeme şartlarınız nelerdir? Nasıl ödeme yapabilirim? İstanbul dışından turunuzu nasıl alabilirim? <span class="collapsed"><i class="icon-plus-circle"></i></span><span class="expanded"><i class="icon-minus-circle"></i></span></a>
                </div>
                <div id="menuone" class="collapse show">
                  <div class="card-body">
                    <p>Tur kayıt esnasında %35 ödeme yaparak, kalan ödemenizi tur hareket tarihine bir ay kala tamamlayabilirsiniz. Ödemelerinizi ofisimize gelmeden de banka hesaplarımıza eft/havaleyle veya kredi kartı mail order formu talep ederek kredi kartıyla yapabilirsiniz. Satışlarımızın %99’u bu yöntemlerle yapılmaktadır. Tüm belgeler elektronik olup mail yoluyla sizlere ulaştırılmaktadır.</p>
                  </div>
                </div>
              </div>

              <div class="card">
                <div class="card-header">
                  <a class="card-link" data-toggle="collapse"  href="#menutwo" aria-expanded="false" aria-controls="menutwo">Satın aldığım tura gelemezsem bir başkası benim yerime katılabilir mi? <span class="collapsed"><i class="icon-plus-circle"></i></span><span class="expanded"><i class="icon-minus-circle"></i></span></a>
                </div>
                <div id="menutwo" class="collapse">
                  <div class="card-body">
                    <p>Tur hareket tarihine 7 gün kalana kadar bu mümkündür ancak isme özel ayırtılan uçak biletlerine ve rezervasyonlara ait tüm cezalar yolcudan tahsil edilmektedir.</p>
                  </div>
                </div>
              </div>

              <div class="card">
                <div class="card-header">
                  <a class="card-link" data-toggle="collapse"  href="#menu3" aria-expanded="false" aria-controls="menu3"> Her gün hareketli tur ne demek? Mesela yılbaşında veya bayramda bu fiyattan gidebilir miyim? <span class="collapsed"><i class="icon-plus-circle"></i></span><span class="expanded"><i class="icon-minus-circle"></i></span></a>
                </div>
                <div id="menu3" class="collapse">
                  <div class="card-body">
                    <p>Her gün hareketli turlarımız seçeceğiniz günde uygun fiyatlama sınıfından uçak müsaitliği olduğu zaman ve bölgede yoğun sezon durumu olmadığı tarihlerde geçerlidir. Dolayısıyla bayram, yılbaşı, noel gibi tarihlerde genellikle fiyatlarda surcharge (fiyat farkı) uygulamaları bulunur.</p>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-md-6">
              <div class="card">
                <div class="card-header">
                  <a class="card-link" data-toggle="collapse"  href="#menu4" aria-expanded="false" aria-controls="menu4">Turunuzda hangi otellerde konaklıyoruz? <span class="collapsed"><i class="icon-plus-circle"></i></span><span class="expanded"><i class="icon-minus-circle"></i></span></a>
                </div>
                <div id="menu4" class="collapse">
                  <div class="card-body">
                    <p>Münferit hareketli (Bayram ve Club turları hariç diğer tüm turlar) turlarda hangi oteli isterseniz onda konaklıyorsunuz. Sitede otel belirtilmeden verilen fiyatlarımız Siamtur standartları içinde yer alan en düşük fiyatlı otelden hazırlanmıştır. Otellerle ilgili detaylı bilgi için bize ulaşabilirsiniz.</p>
                  </div>
                </div>
              </div>

              <div class="card">
                <div class="card-header">
                  <a class="card-link" data-toggle="collapse"  href="#menu5" aria-expanded="false" aria-controls="menu5">Fiyatlarınızı anlamadım. Kişi başı mı? Gecelik otel fiyatı mı? Neler Dahil? <span class="collapsed"><i class="icon-plus-circle"></i></span><span class="expanded"><i class="icon-minus-circle"></i></span></a>
                </div>
                <div id="menu5" class="collapse">
                  <div class="card-body">
                    <p>Balayı turları dahil tüm turlarımızda verdiğimiz fiyatlar her zaman kişi başı toplam tur bedelini ifade eder. Çok seçmeli otel ve oda kategorisi bulunan listelerimizde fiyatlar o otel ve odalama seçildiğinde kişi başı toplam tur fiyatını gösterir.</p>
                  </div>
                </div>
              </div>

              <div class="card">
                <div class="card-header">
                  <a class="card-link" data-toggle="collapse"  href="#menu6" aria-expanded="false" aria-controls="menu6">Acentanız olmak için ne yapmam gerekiyor? <span class="collapsed"><i class="icon-plus-circle"></i></span><span class="expanded"><i class="icon-minus-circle"></i></span></a>
                </div>
                <div id="menu6" class="collapse">
                  <div class="card-body">
                    <p>Türsab belge numaranızla bizlere başvurarak acenta başvuru formu doldurmanız gerekmektedir.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection